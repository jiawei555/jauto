package com.jauto.auto.example;

import com.jauto.auto.core.Device;
import com.jauto.auto.core.Devices;
import com.jauto.auto.core.Node;

import java.util.List;

/**
 * 〈功能概述〉<br>
 *
 * @className: Douyin
 * @package: com.jauto.auto.example
 * @author: zhangRenHuo
 * @date: 2020/9/23 14:11
 */
public class Douyin {

    public static void main(String[] args) throws Exception{
        Douyin douyin = new Douyin();
        List<Device> devices = Devices.getDevices();
        if (null != devices){
            Device device = devices.get(0);
            //找到抖音短视频
            List<Node> nodes = device.find().text("抖音短视频").getNodes();
            if (null != nodes){
                //点击抖音短视频
                nodes.get(0).click();
            }
            while (true){
                // 循环关注
                douyin.attention(device);
            }
        }else {
            System.err.println("未找到设备");
        }

    }

    /*
     * 自动刷视频关注
     * @Author zhangRenHuo
     * @Date 14:15 2020/9/23
     * @param null
     * @return
     */
    private void attention(Device device) throws Exception{
        Device.DeviceSize size = device.getSize();
        device.swipe(size.getWidth()-10,size.getHeigth()/2,0,size.getHeigth()/2);
        List<Node> nodes = device.findMain().text("#  关注").getNodes();
        if (null != nodes){
            nodes.get(0).click();
        }
        device.swipe(10,size.getHeigth()/2,size.getWidth(),size.getHeigth()/2);
        device.swipe(size.getWidth()/2,size.getHeigth()/2,size.getWidth()/2,0);
    }
}
