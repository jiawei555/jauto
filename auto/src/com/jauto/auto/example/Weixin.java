package com.jauto.auto.example;

import com.jauto.auto.core.Device;
import com.jauto.auto.core.Devices;
import com.jauto.auto.core.Node;

import java.util.List;

/**
 * 〈功能概述〉<br>
 *
 * @className: Weixin
 * @package: com.jauto.auto.example
 * @author: zhangRenHuo
 * @date: 2021/1/23 16:20
 */
public class Weixin {

    public static void main(String[] args) throws InterruptedException {
        List<Device> devices = Devices.getDevices();
        if (!devices.isEmpty()){
            Device device = devices.get(0);
            int x =0;
            int y = 0;
            Node node = device.findMain().id("com.tencent.mm:id/ajs").getNodes().get(0);
            while (true){
                node.inputBroadcast("[炸弹]");
                if(x==0){
                    Node node1 = device.find().text("发送").getNodes().get(0);
                    x = node1.getCenterX();
                    y = node1.getCenterY();
                    device.click(x,y);
                }else {
                    device.click(x,y);
                }
            }

        }

    }
}
