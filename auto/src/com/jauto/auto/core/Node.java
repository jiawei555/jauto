package com.jauto.auto.core;

import com.jauto.auto.utils.ShellUtil;

/**
 * 〈功能概述〉<br>
 * 元素
 * @className: Element
 * @package: com.java.auto.core
 * @author: zhangRenHuo
 * @date: 2020/8/5 10:43
 */
public class Node {

    private String deviceCode;
    private String text;
    private String resourceId;
    private String className;
    private String contentDesc;
    private String checkable;
    private String checked;
    private String clickable;
    private String bounds;
    private Integer centerX;
    private Integer centerY;

    //adb shell input text _AvDcG33CZVMyA0LsBVeTFrcdM3e_-7Lrb_b8hzWLHVfbatXTF0Scg

    public void click(){
        shell("shell input tap "+centerX+" "+centerY);
    }

    /*
     * @Description:
     * 输入文本
     * @Author zhangRenHuo
     * @Date 10:53 2020/8/6
     * @return
     **/
    public void input(String text){
        click();
        shell("shell input text '"+text+"'");
    }

    /*
     * @Description:
     * 输入文本--支持中文
     * @Author zhangRenHuo
     * @Date 10:53 2020/8/6
     * @return
     **/
    public void inputBroadcast(String text){
        click();
        shell("shell am broadcast -a ADB_INPUT_TEXT --es msg '"+text+"'");
    }

    private String shell(String adb){
        Process cmd = ShellUtil.cmd("-s " + deviceCode + " " + adb);
        String shellOut = ShellUtil.getShellOut(cmd);
        return shellOut;
    }


    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getContentDesc() {
        return contentDesc;
    }

    public void setContentDesc(String contentDesc) {
        this.contentDesc = contentDesc;
    }

    public String getCheckable() {
        return checkable;
    }

    public void setCheckable(String checkable) {
        this.checkable = checkable;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getClickable() {
        return clickable;
    }

    public void setClickable(String clickable) {
        this.clickable = clickable;
    }

    public String getBounds() {
        return bounds;
    }

    public void setBounds(String bounds) {
        this.bounds = bounds;
    }

    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(Integer centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(Integer centerY) {
        this.centerY = centerY;
    }

}
