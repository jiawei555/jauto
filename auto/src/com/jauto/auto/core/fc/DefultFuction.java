package com.jauto.auto.core.fc;

/**
 * 〈功能概述〉<br>
 *
 * @className: DefultFuction
 * @package: com.java.auto.core.fc
 * @author: zhangRenHuo
 * @date: 2020/8/6 9:41
 */
@FunctionalInterface
public interface DefultFuction<R> {
    R start();
}
