package com.jauto.auto.core.fc;

/**
 * 〈功能概述〉<br>
 *
 * @className: FindFunction
 * @package: com.jauto.auto.core.fc
 * @author: zhangRenHuo
 * @date: 2020/12/21 10:57
 */
@FunctionalInterface
public interface FindFunction<P,R> {
    R start(P p);
}
