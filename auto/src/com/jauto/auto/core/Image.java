package com.jauto.auto.core;

import com.jauto.auto.utils.ShellUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * 〈功能概述〉<br>
 * 设备图片对象--图片找色
 * @className: Images
 * @package: com.jauto.auto.core
 * @author: zhangRenHuo
 * @date: 2020/9/25 10:11
 */
public class Image {
    //图片地址
    private String imagePath;
    //
    private int step = 5;

    private BufferedImage image;

    private int width;
    private int height;
    private int minx;
    private int miny;

    private Image(){}

    public Image(Device device){
        String deviceCode = device.getDeviceCode();
        String tempFile = device.getTempFile();
        this.imagePath = tempFile+deviceCode+".png";
        pullImage(deviceCode);
        File file = new File(this.imagePath);
        try {
            this.image = ImageIO.read(file);
            this.width = image.getWidth();
            this.height = image.getHeight();
            this.minx = image.getMinX();
            this.miny = image.getMinY();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取设备图片到pc端
     * @Author zhangRenHuo
     * @Date 10:28 2020/9/25
     * @param deviceCode 设备id
     * @return
     */
    private void pullImage(String deviceCode){
        ShellUtil.cmd("-s " + deviceCode + " " + "shell /system/bin/screencap -p /sdcard/"+deviceCode+".png");
        ShellUtil.cmd("-s " + deviceCode + " " + "pull /sdcard/"+deviceCode+".png "+imagePath);
    }


    /**
     * 根据rbg值找到第一个像素点坐标  从上到下 从左往右
     * @Author zhangRenHuo
     * @Date 15:06 2020/9/25
     * @param range 幅度
     * @return Point
     */
    public Point downAndright(int r,int b, int g,int range,int step){
        int[] rgb = new int[3];
        for (int x = this.minx; x < this.width; x += step) {
            for (int y = this.miny; y < this.height; y += step) {
                int pixel = this.image.getRGB(x, y); // 下面三行代码将一个数字转换为RGB数字
                rgb[0] = (pixel & 0xff0000) >> 16;
                rgb[1] = (pixel & 0xff00) >> 8;
                rgb[2] = (pixel & 0xff);
                boolean macthRgb = macthRgb(r, b, g, rgb[0], rgb[1], rgb[2], range);
                if (macthRgb){
                    Point point = new Point();
                    point.rgb = rgb[0] + "," + rgb[1] + "," + rgb[2];
                    point.x = x;
                    point.y = y;
                    return point;
                }
            }
        }
        return null;
    }

    /**
     * 从左往右 从上到下 默认
     * @Author zhangRenHuo
     * @Date 17:00 2020/9/25
     * @param
     * @return
     */
    private Point rightAndDown(int r, int b,int g,int range,int step){
        int[] rgb = new int[3];
        for (int y = this.miny; y < this.height; y += step) {
            for (int x = this.minx; x < this.width; x += step) {
                int pixel = this.image.getRGB(x, y);
                rgb[0] = (pixel & 0xff0000) >> 16;
                rgb[1] = (pixel & 0xff00) >> 8;
                rgb[2] = (pixel & 0xff);
                boolean macthRgb = macthRgb(r, b, g, rgb[0], rgb[1], rgb[2], range);
                if (macthRgb){
                    Point point = new Point();
                    point.rgb = rgb[0] + "," + rgb[1] + "," + rgb[2];
                    point.x = x;
                    point.y = y;
                    return point;
                }
            }
        }
        return null;
    }

    /**
     * 默认步距为 5
     * @Author zhangRenHuo
     * @Date 16:44 2020/9/25
     * @param range 颜色差值
     * @return
     */
    public Point getPoint(int r,int b, int g,int range){
        return rightAndDown(r,b,g,range,this.step);
    }

    /**
     * 判断像素是否一样
     * @Author zhangRenHuo
     * @Date 15:18 2020/9/25
     * @return
     */
    private boolean macthRgb(int r,int g, int b,int mr,int mg,int mb,int range){
        if ( Math.abs((r-mr))<=range && Math.abs((g-mg))<=range && Math.abs((b-mb))<=range){
            return true;
        }
        return false;
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        Image rc = new Image();
        Point point = rc.getPoint(190, 110, 117, 1);
        System.err.println(point.rgb);
    }

    class Point{
        private Integer x;
        private Integer y;
        private String rgb;
        public Integer getX() {
            return x;
        }
        public void setX(Integer x) {
            this.x = x;
        }

        public Integer getY() {
            return y;
        }

        public void setY(Integer y) {
            this.y = y;
        }

        public String getRgb() {
            return rgb;
        }

        public void setRgb(String rgb) {
            this.rgb = rgb;
        }
    }

}
