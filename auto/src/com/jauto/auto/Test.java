package com.jauto.auto;

import com.jauto.auto.core.Device;
import com.jauto.auto.core.Devices;


import java.util.List;

/**
 * 〈功能概述〉<br>
 *
 * @className: Test
 * @package: com.java.auto
 * @author: zhangRenHuo
 * @date: 2020/8/5 10:10
 */
public class Test {
    public static void main(String[] args)throws Exception{
        List<Device> devices = Devices.getDevices();
        Device device = devices.get(0);
        device.findMain().text("日历").getNodes().get(0).click();
    }

    public static void println(String str){
        System.out.println(str);
    }
}
