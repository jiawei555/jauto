# JAuto

#### 介绍
java封装adb命令,实现java来编写android手机控制脚本，如抖音自动关注，抖音评论，抖音养号等。只需pc端安装java环境就可使用，不依赖其他第三方库

#### 软件架构
软件架构说明


#### 安装教程

1.  安装java环境
2.  adb连接手机即可使用 (adb.exe命令在 utils目录下 cmd到该目录可执行adb指令)

#### 使用说明

1.  获取device对象  
    Device device = Devices.getDevice("S3V0217913002602");  // 一个设备对象   
    List<Device> devices = Devices.getDevices(); //获取当前电脑连接的所有设备对象
    
2.  获取节点  
    device.find().text("爱奇艺").getNodes(); // 通过text属性查找节点  
    device.find().id("***")getNodes(); // 通过id属性查找节点  
    
3.  操作节点  
    nodes = device.find().text("爱奇艺").getNodes();  
    nodes.get(0).click(); // 点击爱奇艺
    
4.  需要输入中文则要设备安装ADBkeyboard.apk     
    cd /utils       
    adb install ADBkeyboard.apk     
    adb shell ime set com.android.adbkeyboard/.AdbIME       
    node.inputBroadcast("中文输入")     
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
